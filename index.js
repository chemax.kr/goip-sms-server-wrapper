/*
* Как всё должно работать:
* Пользователь авторизуется
* Дальше, мы, имея токен, знаем что этому пользователю позволено.
* Что ему позволено: читать и\или писать с определенных гоип
* Определяется двумя списками в модели пользователя
* Если пользователь авторизован, при загрузке ему присылается три списка.
* 1) Список гоипов к которым он имеет доступ на чтение и\или запись, одним махом:
* 2) Список входящих смс с тех гоипов к которым он имеет доступ
* 3) Список исходящих смс с тех гоипов к которым он имеет доступ
*
* Дальше, человек может отправить смс с гоипа к которому имеет доступ.
* Таблицы с СМС имеют фильтр\поиск по полям текст, от, кому, дата.
* Значит мне нужны следующие методы:
* Запрос разрешений пользователя, вызывается при каждом действии.
* Запрос имен гоипов на которые есть разрешения у пользователя
* Запрос всех смс которые пришли на разрешенные гнезда
* Запрос всех смс которые были отправлены с разрешенных гнезд
* Отправка смс с гоипов из списка. Параметры: ([goip], to, text)
*
* Значит мне нужны две таблицы:
* Входящие смс
* Исходящие смс
*
* 1 форма для отправки с полями: кому, текст, выбор гоипа (предполагаю мультиселект список).
*
* Для пользователей отмеченных тегом "Админ" нужна таблица с перечнем юзеров и гоипов, в которой можно будет ставить галочки - читать\писать\запрещено.
* */

const Koa = require('koa'); // ядро
const config = require('./libs/config'); //конфиг
const fn = require('./libs/fn'); //функции
const Router = require('koa-router'); // маршрутизация
const bodyParser = require('koa-bodyparser'); // парсер для POST запросов
const serve = require('koa-static'); // модуль, который отдает статические файлы типа index.html из заданной директории
const logger = require('koa-logger'); // опциональный модуль для логов сетевых запросов. Полезен при разработке.

const passport = require('koa-passport'); //реализация passport для Koa
const LocalStrategy = require('passport-ldapauth');
const JwtStrategy = require('passport-jwt').Strategy; // авторизация через JWT
const ExtractJwt = require('passport-jwt').ExtractJwt; // авторизация через JWT

const jwtsecret = "mysecretkey"; // ключ для подписи JWT
const jwt = require('jsonwebtoken'); // аутентификация  по JWT для hhtp
const socketioJwt = require('socketio-jwt'); // аутентификация  по JWT для socket.io

const socketIO = require('socket.io');
// const mysql = require('mysql');

const crypto = require('crypto'); // модуль node.js для выполнения различных шифровальных операций, в т.ч. для создания хэшей.

const app = new Koa();
const router = new Router();

// let connection = mysql.createConnection(config.get('mysql'));
// connection.connect();


app.use(serve('public'));
app.use(logger());
app.use(bodyParser());

app.use(passport.initialize()); // сначала passport
app.use(router.routes()); // потом маршруты
const server = app.listen(config.get('port'));// запускаем сервер на порту 3000

// fn.checkUserRight('chemax');

/*========*/
let OPTS = config.get('ldap-opts');

/*========*/


//----------Passport Local Strategy--------------//

passport.use(new LocalStrategy(OPTS));

//----------Passport JWT Strategy--------//

// Ждем JWT в Header

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    secretOrKey: jwtsecret
};


//------------Routing---------------//

//маршрут для авторизации и создания JWT при успешной авторизации

router.post('/login', async (ctx, next) => {
    await passport.authenticate('ldapauth', function (err, user) {
        if (user == false) {
            ctx.body = "Login failed";
        } else {
            //--payload - информация которую мы храним в токене и можем из него получать
            const payload = {
                id: user.id,
                displayName: user.displayName,
                email: user.email
            };
            const token = jwt.sign(payload, jwtsecret); //здесь создается JWT
            // 'JWT ' +
            ctx.body = {user: user.displayName, token: token};
        }
    })(ctx, next);

});


//---Socket Communication-----//
let io = socketIO(server);

io.on('connection', socketioJwt.authorize({
    secret: jwtsecret,
    timeout: 15000
})).on('authenticated', function (socket) {

    socket.emit('auth-ok', "Авторизация успешна, токен принят");
    // console.log('Это мое имя из токена: ' + socket.decoded_token.displayName);
    // fn.getSms(socket);
    // fn.allowedGoip(socket);
    socket.on('getSms', function () {
        console.log('get state');
        fn.getSms(socket);
    });

    socket.on('sendSms', function (msg) {
        console.log('socket emit send sms');
        fn.trySendSMS(socket, msg);
    });

    socket.on('getAllowedGoip', function () {
        fn.allowedGoip(socket);
    });

    socket.on("clientEvent", (data) => {
        console.log(data);
    })
});