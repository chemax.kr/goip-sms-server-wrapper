/**
 * Created by chemax on 08.08.2017.
 */
const mysql = require('mysql');
const config = require('./config'); //конфиг
const fetch = require('node-fetch');
let connection = mysql.createConnection(config.get('mysql'));
connection.connect();

module.exports.checkUserRight = function (socket) {
    connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
        if (error) throw error;
        // console.log('The solution is: ', results[0].solution);
    });
    // console.log(socket.decoded_token.displayName);
};

module.exports.getSms = function (socket) {
    getUserRight(socket, getRecievedSmsFor);
    getUserRight(socket, getSendedSmsFor);
    getUserRight(socket, sendAllowedGoip);
};

function getSendedSmsFor(socket, userRights) {
    let canRead = "";
    for (i in userRights) {
        if (userRights[i].isCanRead > 0) {
            canRead += ", " + userRights[i].goipId;
        }
    }

    let sql = "SELECT sends.id, sends.time, message.msg, prov.prov, message.tel FROM sends " +
        "LEFT JOIN message ON (message.id = sends.messageid) " +
        "LEFT JOIN prov ON (prov.id = sends.provider) " +
        "LEFT JOIN goip ON (goip.id = sends.goipid) " +
        "WHERE sends.goipid in (" + canRead.replace(/^, /, "") + ") " +
        " ORDER BY sends.time DESC";
    console.log('sended sms: ', sql);
    connection.query(sql, function (err, results, fields) {
        socket.emit('sendedSMS', results);
    });
    // console.log('get sms for ', socket.decoded_token.displayName);
}

module.exports.trySendSMS = function (socket, msg) {
    console.log('try to send sms', msg);
    socket.msg = msg;
    getUserRight(socket, sendSMS);


}

function sendSMS(socket, userRights) {
    'use strict';
    let msg = socket.msg;
    let regexp = /.+USERNAME.+/;
    let sql = "SELECT provider FROM goip WHERE id = '" + msg.goip + "'";
    let canSend = [];
    for (let i in userRights) {
        if (userRights[i].isCanSend > 0) {
            canSend.push(userRights[i].goipId)

        } else {

        }
    }
    console.log(canSend);
    if (canSend.indexOf(msg.goip) > -1) {
        console.log('rights for send sms ok');
        connection.query(sql, function (error, sqlresult, fields) {

            let url = config.get('sendSmsUrl') +
                "dosend.php?USERNAME=sender&PASSWORD=12z5y8t7a" +
                "&smsprovider=" + sqlresult[0].provider +
                "&smsgoip=" + msg.goip +
                "&smsnum=" + encodeURIComponent(msg.number) +
                "&method=2" +
                "&Memo=" + encodeURIComponent(msg.msg)
            ;
            console.log('url: ', url);
            let result = "";
            // let url = "http://192.168.1.24/goip/en/dosend.php?smsnum=%2B79237590030&smsgoip=76&smsprovider=2&Memo=dsadasds&USERNAME=root&PASSWORD=root&method=2";
            // console.log(url);
            // console.log('send sms to', msg.smsnum);
            fetch(url)
                .then(response => response.text())
                .then(text => {
                    console.log('=========');
                    console.log(text);
                    console.log('=========');
                    result = text.replace(/.+window.location = '/, "").replace(/'<\/script>/, "");
                    // console.log(regexp.exec(result)[0])
                    let urlResend = config.get('sendSmsUrl') + regexp.exec(result)[0];
                    fetch(urlResend)
                        .then(response => response.text())
                        .then(text => {
                            // console.log(text)
                        })
                        .catch(err => console.error(err));

                })
                .catch(err => console.error(err));
        });
    }
    else{
        console.log('rights for send sms bad');
    }
}

module.exports.allowedGoip = function (socket) {
    getUserRight(socket, sendAllowedGoip)
};

function sendAllowedGoip(socket, data) {
    let allowedGoipStr = "";
    for (i in data) {
        if (data[i].isCanSend == 1) allowedGoipStr = allowedGoipStr + ", " + data[i].goipId;
    }
    let sql = "SELECT * FROM goip WHERE id in (" + allowedGoipStr.replace(/^, /, "") + ")";
    // console.log(sql);
    connection.query(sql, function (error, results, fields) {
        socket.emit('allowedGoip', results);
    });

}

function getRecievedSmsFor(socket, userRights) {

    let canRead = "";
    for (i in userRights) {
        if (userRights[i].isCanRead > 0) {
            canRead += ", " + userRights[i].goipId;
        }
    }

    let sql = "SELECT " +
        "receive.id, receive.srcnum, receive.msg, receive.time, receive.goipname, receive.goipid, goip.provider" +
        " FROM receive " +
        "LEFT JOIN goip ON (goip.id=receive.goipid) " +
        // "LEFT JOIN goip ON (goip.name=receive.goipname) " +
        "WHERE goipid in (" + canRead.replace(/^, /, "") + ") " +
        "ORDER by receive.time DESC " +
        "LIMIT 1000";
    // let sql = "SELECT * FROM receive ORDER BY time DESC";
    console.log(sql);
    connection.query(sql, function (err, results, fields) {
        socket.emit('recievedSms', results);
    });
    // console.log('get sms for ', socket.decoded_token.displayName);
}

function getUserRight(socket, callback) {
    let sql = 'SELECT * FROM user_rights WHERE username = \'' + socket.decoded_token.displayName + '\'';
    connection.query(sql, function (error, results, fields) {
        return callback(socket, results);
    });
}
