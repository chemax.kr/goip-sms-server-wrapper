/*========*/
const config = require('./libs/config');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const passport     = require('passport');
const LdapStrategy = require('passport-ldapauth');
/*========*/


/*========*/
var OPTS = {
    server: {
        url: 'ldap://192.168.1.23:389',
        bindDN: 'CN=chemax,CN=Users,DC=trans,DC=local',
        bindCredentials: 'Che135711',
        searchBase: 'CN=Users,DC=trans,DC=local',
        // searchFilter: '(uid={{sAMAccountName}})'
        searchFilter: '(sAMAccountName={{username}})'
    }
};
/*========*/



/*========*/
// const urlencodedParser = bodyParser.urlencoded({extended: false});
// const jsonParser = bodyParser.json();
/*========*/
//
// server.listen(config.get('port'), function () {
//     console.log('listening on *:' + config.get('port'));
// });


/*=========*/
passport.use(new LdapStrategy(OPTS));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(passport.initialize());
// app.use(express.static(__dirname + '/public'));
app.post('/login', passport.authenticate('ldapauth', {session: false}), function(req, res) {
    res.send({status: 'ok'});
});
app.listen(8080);
/*=============*/