let loginform = new Vue({
    el: "#loginform",
    data: {
        seen: true,
        login: localStorage.getItem('login'),
        password: ""
    },
    methods: {
        trylogin: function () {
            // console.log(this.login, this.password);
            var obj = {
                method: 'POST',
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                body: 'username=' + this.login + '&password=' + this.password
            };
            // console.log(obj);
            fetch("/login", obj).then(function (response) {
                return response.text()
            }, function (error) {
                error.message //=> String
            }).then(function (text) {
                // console.log(text);
                let token = JSON.parse(text);
                // console.log(token.token);
                loginform.seen = false;
                socket.emit('getSms');
                console.log('successfully login');
                localStorage.setItem('token', token.token);
                localStorage.setItem('login', token.user);
            });
        },
        logout: function () {
            localStorage.removeItem('token');
            localStorage.removeItem('login');
            recievedSMS.smses = [];
            sendedSMS.smses = [];
            smsSender.options = [];
            loginform.seen = true;
            // console.log('remove token');
        }
    }
});

let recievedSMS = new Vue({
    el: '#receivedsms',
    data: {
        smses: []
    },
    methods: {
        reply: function (sms) {
            console.log(sms);
            smsSender.number = sms.srcnum;
            console.log(sms.goipid);
            smsSender.goip = sms.goipid;
        }
    }
});

let smsSender = new Vue({
    el: "#sendsmsdiv",
    data: {
        number: "",
        textsms: "",
        goip: "",
        options: []
    },
    methods:{
        sendsms: function () {
            let dataset = {
                msg: this.textsms,
                number: this.number,
                goip: this.goip
            };
            this.textsms = "";
            this.number = "";
            console.log(dataset);
            socket.emit('sendSms', dataset);
        }
    }
});

let sendedSMS = new Vue ({
    el: "#sendedsms",
    data: {
        smses: []
    }
});